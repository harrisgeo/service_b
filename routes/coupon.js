var express = require('express')
var router = express.Router()
var _ = require('lodash')
var jsonfile = require('jsonfile')
var file = './data.json'

jsonfile.readFile(file, function(err, obj) {
	router.get('', function(req, res) {
		var coupon = obj.shift() // remove 1 coupon from array
		jsonfile.writeFile(file, obj, function(err) {
			res.send(coupon) // return coupon
		})
	})
})

module.exports = router;