var express = require('express')
var router = express.Router()
var _ = require('lodash')
var jsonfile = require('jsonfile')
var file = './data.json'

var storage = [{"brand":"tesco","value":"20"},{"brand":"sainsburys","value":"5"},{"brand":"waitrose","value":"10"},{"brand":"tesco","value":"10"},{"brand":"sainsburys","value":"15"},{"brand":"waitrose","value":"20"},{"brand":"tesco","value":"25"},{"brand":"sainsburys","value":"25"},{"brand":"waitrose","value":"25"},{"brand":"tesco","value":"10"},{"brand":"sainsburys","value":"25"},{"brand":"waitrose","value":"20"}]

// re-add all the data in the database
router.get('', function(req, res) {
	jsonfile.writeFile(file, storage, function(err) {
		jsonfile.readFile(file, function(err, obj) {
			res.send(obj)
		})
	})
})

module.exports = router;