var express = require('express')
var router = express.Router()

router.get('/products/:brand/:value', function(req, res) {
	var {brand, value} = req.params
	res.send(JSON.stringify({brand: brand, value: value}))
})

module.exports = router;