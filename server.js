var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// routes
app.use('/refresh-storage', require('./routes/refresh_storage'));
app.use('/api', require('./routes/api'));
app.use('/get-coupon', require('./routes/coupon'));


// start server
app.listen(3000)
console.log('API is running on port 3000');