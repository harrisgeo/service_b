# Service B developed in Node JS

To install

`npm install`

to run

`nodemon server.js`

Memory data-storage is a json file that includes a number of tickets

# To get a coupon visit

`http://127.0.0.1:3000/get-coupon`

The coupon that is returned is removed from the data-storage


# To refresh the storage memory in case the coupons finish run

`http://127.0.0.1:3000/refresh-storage`
